package H05;
import java.util.Scanner;
public class Homework05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int minDigit = a % 10;
            while (a != -1) {
                if (a == 0) {
                    minDigit = 0;
                }
                    while (a != 0) {
                        int lastDigit = a % 10;
                        if (lastDigit < minDigit) {
                            minDigit = lastDigit;
                        }
                    a = a / 10;
                    }
                    a = scanner.nextInt();
            }
        System.out.println("Result - " + minDigit);
    }

}



